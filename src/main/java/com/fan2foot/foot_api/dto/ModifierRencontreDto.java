package com.fan2foot.foot_api.dto;

import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.sql.Time;
import java.util.Date;
@Data
@NoArgsConstructor
public class ModifierRencontreDto {
    @Temporal(TemporalType.DATE)
    private Date date;
    private Time heure;
}

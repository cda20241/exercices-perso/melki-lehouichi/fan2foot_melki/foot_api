package com.fan2foot.foot_api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
public class ResultatDto {
    private Long idRencontre;
    private List<Long> butteurs;
    private List<Long> passeurs;
    private Integer scoreDomicile;
    private Integer scoreExterieur;
}

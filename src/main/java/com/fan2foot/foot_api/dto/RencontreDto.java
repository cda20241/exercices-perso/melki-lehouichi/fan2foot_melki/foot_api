package com.fan2foot.foot_api.dto;

import com.fan2foot.foot_api.models.JoueurModel;
import com.fan2foot.foot_api.models.RencontreModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class RencontreDto extends BaseRencontreDto {
    private EquipeDto eqDomicile;
    private EquipeDto eqExterieur;
    private List<JoueurDto> buts = new ArrayList<>();
    private List<JoueurDto> passeD = new ArrayList<>();
    public void mapper(RencontreModel rencontre){
        super.mapper(rencontre);
        this.eqDomicile = new EquipeDto();
        this.eqDomicile.mapper(rencontre.getEqDomicile());
        this.eqExterieur = new EquipeDto();
        this.eqExterieur.mapper(rencontre.getEqExterieur());
        for (JoueurModel joueur : rencontre.getButs()){
            JoueurDto joueurDto = new JoueurDto();
            joueurDto.mapper(joueur);
            this.buts.add(joueurDto);
        }
        for (JoueurModel joueur : rencontre.getPasseD()){
            JoueurDto joueurDto = new JoueurDto();
            joueurDto.mapper(joueur);
            this.passeD.add(joueurDto);
        }
    }

}

package com.fan2foot.foot_api.dto;

import com.fan2foot.foot_api.models.EquipeModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseEquipeDto {
    private Long id;

    private String nomLong;

    private String nomCourt;

    private String championnat;

    public void mapper(EquipeModel equipe){
        this.id=equipe.getId();
        this.nomLong=equipe.getNomLong();
        this.nomCourt=equipe.getNomCourt();
        this.championnat=equipe.getChampionnat();
    }

}

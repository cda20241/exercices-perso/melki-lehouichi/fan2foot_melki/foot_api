package com.fan2foot.foot_api.dto;


import com.fan2foot.foot_api.models.EquipeModel;
import com.fan2foot.foot_api.models.JoueurModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class EquipeDto extends BaseEquipeDto {
    private List<BaseJoueurDto> joueurs = new ArrayList<>();
    public void mapper(EquipeModel equipe){
        super.mapper(equipe);
        for (JoueurModel joueur : equipe.getJoueurs()){
            BaseJoueurDto joueurDto = new BaseJoueurDto();
            joueurDto.mapper(joueur);
            this.joueurs.add(joueurDto);
        }
    }
}

package com.fan2foot.foot_api.dto;

import com.fan2foot.foot_api.models.JoueurModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JoueurDto extends BaseJoueurDto {
    private BaseEquipeDto equipe;

    public void mapper(JoueurModel joueur){
        super.mapper(joueur);
        this.equipe=new BaseEquipeDto();
        this.equipe.mapper(joueur.getEquipe());
    }
}

package com.fan2foot.foot_api.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Data
@Entity
@NoArgsConstructor
@Table(name = "rencontre")
public class RencontreModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "date_rencontre")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "heure_rencontre")
    private Time heure;

    @ManyToOne()
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = "id_eq_dom", referencedColumnName = "id")
    private EquipeModel eqDomicile;

    @Column(name = "score_domicile")
    private Integer scoreDomicile;

    @ManyToOne()
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = "id_eq_ext", referencedColumnName = "id")
    private EquipeModel eqExterieur;

    @Column(name = "score_exterieur")
    private Integer scoreExterieur;

    @ManyToMany
    @JoinTable(
            name = "but",
            joinColumns = @JoinColumn(name = "id_rencontre"),
            inverseJoinColumns  = @JoinColumn(name = "id_joueur"))
    private List<JoueurModel> buts = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "passeD",
            joinColumns = @JoinColumn(name = "id_rencontre"),
            inverseJoinColumns  = @JoinColumn(name = "id_joueur"))
    private List<JoueurModel> passeD = new ArrayList<>();
}

package com.fan2foot.foot_api.enums;

public enum Position {
    GARDIEN,
    DIFFENSEUR,
    MILIEU,
    ATTAQUANT,
}

package com.fan2foot.foot_api.controllers;

import com.fan2foot.foot_api.dto.CreerRencontreDto;
import com.fan2foot.foot_api.dto.ModifierRencontreDto;
import com.fan2foot.foot_api.dto.RencontreDto;
import com.fan2foot.foot_api.dto.ResultatDto;
import com.fan2foot.foot_api.services.RencontreService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rencontre")
@RequiredArgsConstructor
@CrossOrigin("http://localhost:4200")
public class RencontreController {
    @Autowired
    private final RencontreService rencontreService;

    /** ------------------ ROUTES ------------------------------------- */

    @PostMapping("/creer")
    public ResponseEntity<String> creerRencontre(@RequestBody CreerRencontreDto rencontre){
        return rencontreService.creerRencontre(rencontre);
    }
    @GetMapping("/liste")
    public List<RencontreDto> afficherListeJoueurs(){
        return rencontreService.afficherListeRencontres();
    }
    @GetMapping("/{id}")
    public RencontreDto afficherJoueur(@PathVariable Long id){
        return rencontreService.afficherRencontre(id);
    }
    @PutMapping("/modifier/{id}")
    public ResponseEntity<String> modifierJoueur(@PathVariable Long id, @RequestBody ModifierRencontreDto rencontre){
        return rencontreService.modifierRencontre(id, rencontre);
    }
    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<String> supprimerJoueur(@PathVariable Long id){
        return rencontreService.supprimerRencontre(id);
    }

    @PostMapping("/resultat")
    public ResponseEntity<String> ajouterResultat(@RequestBody ResultatDto resultat){
        return rencontreService.ajouterResultatRencontre(resultat);
    }
    @GetMapping("/lancer")
    public ResponseEntity<String> lancerMatchs(){return rencontreService.creerListMatch();}

}

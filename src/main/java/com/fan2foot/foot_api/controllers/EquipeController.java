package com.fan2foot.foot_api.controllers;

import com.fan2foot.foot_api.dto.BaseEquipeDto;
import com.fan2foot.foot_api.dto.EquipeDto;
import com.fan2foot.foot_api.models.EquipeModel;
import com.fan2foot.foot_api.services.EquipeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/equipe")
@RequiredArgsConstructor
@CrossOrigin("http://localhost:4200")
public class EquipeController {
    @Autowired
    private final EquipeService equipeService;

    /** ------------------ ROUTES ------------------------------------- */

    @PostMapping("/creer")
    public ResponseEntity<String> creerEquipe(@RequestBody EquipeModel equipe){
        return equipeService.creerEquipe(equipe);
    }
    @GetMapping("/liste")
    public List<EquipeDto> afficherListeEquipes(){
        return equipeService.afficherListeEquipes();
    }
    @GetMapping("/equipes_sans_joueurs")
    public List<BaseEquipeDto> listeEquipes(){
        return equipeService.listeEquipesSansJoueurs();
    }
    @GetMapping("/{id}")
    public EquipeDto afficherEquipe(@PathVariable Long id){
        return equipeService.afficherEquipe(id);
    }
    @PutMapping("/modifier/{id}")
    public ResponseEntity<String> modifierEquipe(@PathVariable Long id, @RequestBody EquipeModel equipe){
        return equipeService.modifierEquipe(id, equipe);
    }
    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<String> supprimerEquipe(@PathVariable Long id){
        return equipeService.supprimerEquipe(id);
    }
}

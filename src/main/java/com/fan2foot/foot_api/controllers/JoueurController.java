package com.fan2foot.foot_api.controllers;

import com.fan2foot.foot_api.dto.JoueurDto;
import com.fan2foot.foot_api.models.JoueurModel;
import com.fan2foot.foot_api.services.JoueurService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/joueur")
@RequiredArgsConstructor
@CrossOrigin("http://localhost:4200")
public class JoueurController {
    @Autowired
    private final JoueurService joueurService;


    /** ------------------ ROUTES ------------------------------------- */

    @PostMapping("/creer")
    public ResponseEntity<String> creerJoueur(@RequestBody JoueurModel joueur){
        return joueurService.creerJoueur(joueur);
    }
    @GetMapping("/liste")
    public List<JoueurDto> afficherListeJoueurs(){
        return joueurService.afficherListeJoueurs();
    }
    @GetMapping("/{id}")
    public JoueurDto afficherJoueur(@PathVariable Long id){
        return joueurService.afficherJoueur(id);
    }
    @PutMapping("/modifier/{id}")
    public ResponseEntity<String> modifierJoueur(@PathVariable Long id,@RequestBody JoueurModel joueur){
        return joueurService.modifierJoueur(id, joueur);
    }
    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<String> supprimerJoueur(@PathVariable Long id){
        return joueurService.supprimerJoueur(id);
    }
}

package com.fan2foot.foot_api.services;

import com.fan2foot.foot_api.dto.CreerRencontreDto;
import com.fan2foot.foot_api.dto.ModifierRencontreDto;
import com.fan2foot.foot_api.dto.RencontreDto;
import com.fan2foot.foot_api.dto.ResultatDto;
import com.fan2foot.foot_api.models.EquipeModel;
import com.fan2foot.foot_api.models.JoueurModel;
import com.fan2foot.foot_api.models.RencontreModel;
import com.fan2foot.foot_api.repositorys.EquipeRepository;
import com.fan2foot.foot_api.repositorys.JoueurRepository;
import com.fan2foot.foot_api.repositorys.RencontreRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Service pour la gestion des rencontres entre équipes.
 */
@Service
@RequiredArgsConstructor
@CrossOrigin()
public class RencontreService {
    @Autowired
    private final RencontreRepository rencontreRepository;
    @Autowired
    private final EquipeRepository equipeRepository;
    @Autowired
    private final JoueurRepository joueurRepository;

    /**
     * Crée une nouvelle rencontre entre deux équipes.
     *
     * @param creerRencontre Les détails de la rencontre à créer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> creerRencontre(CreerRencontreDto creerRencontre){
        try {
            EquipeModel equipe1 = equipeRepository.findById(creerRencontre.getIdEqDomicile()).orElse(null);
            EquipeModel equipe2 = equipeRepository.findById(creerRencontre.getIdEqExterieur()).orElse(null);
            // vérifier si les équipes existent
            if (equipe1 == null || equipe2 == null ){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Au moins une des équipes n'existe pas !");
                // vérifier si les équipes sont différentes
            } else if (equipe1 == equipe2){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Une équipe ne peut pas s'affronter à elle même !");
                    }
            //Si tout est bon créer la rencontre
                RencontreModel rencontre = new RencontreModel();
                rencontre.setDate(creerRencontre.getDate());
                rencontre.setHeure(creerRencontre.getHeure());
                rencontre.setEqDomicile(equipe1);
                rencontre.setEqExterieur(equipe2);
                rencontre.setScoreDomicile(0);
                rencontre.setScoreExterieur(0);
                rencontre.setButs(new ArrayList<>());
                rencontre.setPasseD(new ArrayList<>());
                rencontreRepository.save(rencontre);
                return ResponseEntity.ok("La rencontre n° : "+ rencontre.getId() + " a été créé avec succès.");
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Une erreur s'est produite lors de la création de la rencontre.");
            }
    }

    /**
     * Affiche les détails d'une rencontre.
     *
     * @param id L'identifiant de la rencontre.
     * @return RencontreDto Les détails de la rencontre.
     */
    public RencontreDto afficherRencontre(Long id){
        RencontreModel rencontre = rencontreRepository.findById(id).orElse(null);
        // Vérifier si la rencontre existe
        if (rencontre != null){
            RencontreDto rencontreDto = new RencontreDto();
            rencontreDto.mapper(rencontre);
            return rencontreDto;
        }
        return null;
    }

    /**
     * Affiche la liste de toutes les rencontres.
     *
     * @return List<RencontreDto> La liste des rencontres.
     */
    public List<RencontreDto> afficherListeRencontres(){
        List<RencontreModel> lesRencontres = rencontreRepository.findAll();
        //Creer une liste de DTO ensuite parcourir les rencontres afin de les mapper
        //Retourner DTO afin d'éviter les problèmes de récursivité
        List<RencontreDto> resultat = new ArrayList<>();
        for (RencontreModel rencontre : lesRencontres){
            RencontreDto rencontreDto = new RencontreDto();
            rencontreDto.mapper(rencontre);
            resultat.add(rencontreDto);
        }
        return resultat;
    }

    /**
     * Modifie les informations d'une rencontre existante.
     *
     * @param id       L'identifiant de la rencontre à modifier.
     * @param rencontre Les nouvelles informations de la rencontre.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> modifierRencontre(Long id, ModifierRencontreDto rencontre){
        try {
            RencontreModel newrencontre = rencontreRepository.findById(id).orElse(null);
            // Vérifier si la rencontre existe
            if (newrencontre == null){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Cette rencontre n'existe pas !");
                }
            //si existe passer à la modification de la date et l'heure
            newrencontre.setDate(rencontre.getDate());
            newrencontre.setHeure(rencontre.getHeure());
            rencontreRepository.save(newrencontre);
            return ResponseEntity.ok("La rencontre n° : "+ newrencontre.getId() + " a été modifiée avec succès.");
            }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la modification de la rencontre.");
        }

    }

    /**
     * Ajoute un résultat à une rencontre.
     *
     * @param resultat Les détails du résultat à ajouter.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> ajouterResultatRencontre(ResultatDto resultat){
        try {
            // récupérer la rencontre en question
            RencontreModel rencontre = rencontreRepository.findById(resultat.getIdRencontre()).orElse(null);
            if (rencontre != null) {
                rencontre.setScoreDomicile(resultat.getScoreDomicile());
                rencontre.setScoreExterieur(resultat.getScoreExterieur());
                // Parcourir le tableau de butteurs afin de créer des buts
                for (Long but : resultat.getButteurs()){
                    JoueurModel joueur = joueurRepository.findById(but).orElse(null);
                    rencontre.getButs().add(joueur);
                }
                // Parcourir le tableau de passeurs afin de créer des passeD
                for (Long passeD : resultat.getPasseurs()){
                    JoueurModel joueur = joueurRepository.findById(passeD).orElse(null);
                    rencontre.getPasseD().add(joueur);        }
                rencontreRepository.save(rencontre);
                return ResponseEntity.ok("Le résultat a été ajouté avec succès.");
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cette rencontre n'existe pas dans la base de données.");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de l'ajout du résultat'");
        }
    }

    /**
     * Supprime une rencontre.
     *
     * @param id L'identifiant de la rencontre à supprimer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> supprimerRencontre(Long id){
        try {
            RencontreModel rencontre = rencontreRepository.findById(id).orElse(null);
            // Vérifier si la rencontre existe déjà
            if (rencontre == null){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Cette rencontre n'existe pas'");
            }
            rencontre.getPasseD().clear();
            rencontre.getButs().clear();
            rencontre.setEqDomicile(null);
            rencontre.setEqExterieur(null);
            rencontreRepository.delete(rencontre);
            return ResponseEntity.ok("L'élément a été supprimé avec succès.");
        } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de la suppression de l'élément.");
            }
    }

    /**
     * Crée une liste de matchs à partir d'un fichier JSON.
     *
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> creerListMatch(){
        // lire le fichier JSON
        Resource resource = new ClassPathResource("match.json");
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream inputStream = resource.getInputStream()) {
            // extraire les données dans une listes typée
            List<CreerRencontreDto> matchs = Arrays.asList(objectMapper.readValue(inputStream, CreerRencontreDto[].class));
            // Parcourir la liste afin de créer les rencontres
            for (CreerRencontreDto match : matchs){
                this.creerRencontre(match);
            }
            return ResponseEntity.ok("Les matchs on été créés avec succès.");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de la création des matchs.");
        }
    }
}

package com.fan2foot.foot_api.services;

import com.fan2foot.foot_api.dto.EquipeJsonDto;
import com.fan2foot.foot_api.dto.JoueurJsonDto;
import com.fan2foot.foot_api.models.EquipeModel;
import com.fan2foot.foot_api.models.JoueurModel;
import com.fan2foot.foot_api.repositorys.EquipeRepository;
import com.fan2foot.foot_api.repositorys.JoueurRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Service pour l'importation de données d'équipes et de joueurs depuis un fichier JSON.
 */
@Service
public class ImportDonnees {
    private final EquipeRepository equipeRepository;
    private final JoueurRepository joueurRepository;
    public ImportDonnees(EquipeRepository equipeRepository, JoueurRepository joueurRepository) {
        this.equipeRepository = equipeRepository;
        this.joueurRepository = joueurRepository;
    }

    /**
     * Crée des équipes et des joueurs en utilisant les données d'un fichier JSON.
     *
     * @throws IOException Si une erreur d'entrée-sortie se produit lors de la lecture du fichier JSON.
     */
    public void creerEquipesJoueurs() throws IOException {
        // Lecture du fichier JSON contenant les données des équipes et des joueurs
        Resource resource = new ClassPathResource("data.json");
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream inputStream = resource.getInputStream()) {
            List<EquipeJsonDto> equipes = Arrays.asList(objectMapper.readValue(inputStream, EquipeJsonDto[].class));
            // initier à -22 afin de permettre l'incrémentation et différencier les ID de JSON
            Integer idJson = -22;
            // Parcours de chaque équipe du fichier JSON
            for (EquipeJsonDto equipe : equipes){
                EquipeModel newEquipe = new EquipeModel();
                newEquipe.setId(equipe.getId_club());
                newEquipe.setNomLong(equipe.getLong_name());
                newEquipe.setNomCourt(equipe.getShort_name());
                newEquipe.setChampionnat("Première Ligue Ecosse");
                // Enregistrement de l'équipe dans la base de données
                newEquipe = equipeRepository.save(newEquipe);
                //incrémenter le ID pour chaque équipe
                idJson+=22;
                // Parcourir chaque joueur de l'équipe
                for (JoueurJsonDto joueur : equipe.getListe_joueur()){
                    JoueurModel newJoueur = new JoueurModel();
                    newJoueur.setId(joueur.getId_joueur()+idJson);
                    newJoueur.setEquipe(newEquipe);
                    newJoueur.setAge(joueur.getAge());
                    newJoueur.setNom(joueur.getNom());
                    newJoueur.setNumero(joueur.getNumero());
                    newJoueur.setPosition(joueur.getPosition());
                    newJoueur.setPrenom(joueur.getPrenom());
                    newJoueur.setNomCourt(joueur.getShort_name());
                    newJoueur.setPoids(joueur.getPoids());
                    newJoueur.setTaille(joueur.getTaille());
                    newJoueur.setNationalite(joueur.getNationalite());
                    // Enregistrement du joueur dans la base de données
                    joueurRepository.save(newJoueur);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Gérer l'exception de manière appropriée
        }
    }
}

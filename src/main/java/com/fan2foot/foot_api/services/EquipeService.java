package com.fan2foot.foot_api.services;

import com.fan2foot.foot_api.dto.BaseEquipeDto;
import com.fan2foot.foot_api.dto.EquipeDto;
import com.fan2foot.foot_api.models.EquipeModel;
import com.fan2foot.foot_api.models.JoueurModel;
import com.fan2foot.foot_api.repositorys.EquipeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Service pour la gestion des équipes.
 */
@Service
@RequiredArgsConstructor
public class EquipeService {

    @Autowired
    private final EquipeRepository equipeRepository;

    /**
     * Crée une nouvelle équipe.
     *
     * @param equipe L'équipe à créer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> creerEquipe(EquipeModel equipe){
        try {
            if (equipe.getId()==null){
                equipeRepository.save(equipe);
                return ResponseEntity.ok("L'équipe' "+ equipe.getNomLong() + " a été créée avec succès.");
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cett équipe contient un ID, il se peut qu'elle existe déjà !");
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la création de l'équipe.");
        }
    }

    /**
     * Affiche les détails d'une équipe.
     *
     * @param id L'identifiant de l'équipe.
     * @return EquipeDto Les détails de l'équipe.
     */
    public EquipeDto afficherEquipe(Long id){
        EquipeModel equipe = equipeRepository.findById(id).orElse(null);
        if (equipe!=null){
            /** utilisation de DTO pour éviter les problème de recursivité */
            EquipeDto equipeDto = new EquipeDto();
            equipeDto.mapper(equipe);
            return equipeDto;
        }
        return null;
    }

    /**
     * Affiche la liste de toutes les équipes.
     *
     * @return List<EquipeDto> La liste des équipes.
     */
    public List<EquipeDto> afficherListeEquipes(){
        List<EquipeModel> lesEquipes = equipeRepository.findAll();
        List<EquipeDto> resultat = new ArrayList<>();
        for (EquipeModel eq : lesEquipes){
            /** utilisation de DTO pour éviter les problème de recursivité */
            EquipeDto equipe = new EquipeDto();
            equipe.mapper(eq);
            resultat.add(equipe);
        }
        return resultat;
    }

    /**
     * Affiche la liste des équipes sans les détails des joueurs.
     *
     * @return List<BaseEquipeDto> La liste des équipes sans joueurs.
     */
    public List<BaseEquipeDto> listeEquipesSansJoueurs(){
        List<EquipeModel> lesEquipes = equipeRepository.findAll();
        List<BaseEquipeDto> resultat = new ArrayList<>();
        for (EquipeModel eq : lesEquipes){
            BaseEquipeDto equipe = new BaseEquipeDto();
            equipe.mapper(eq);
            resultat.add(equipe);
        }
        return resultat;
    }

    /**
     * Modifie les informations d'une équipe existante.
     *
     * @param id     L'identifiant de l'équipe à modifier.
     * @param equipe Les nouvelles informations de l'équipe.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> modifierEquipe(Long id, EquipeModel equipe){
        try {
            EquipeModel newequipe = equipeRepository.findById(id).orElse(null);
            if (newequipe!=null){
                equipe.setId(id);
                equipeRepository.save(equipe);
                return ResponseEntity.ok("L'équipe' "+ equipe.getNomLong() + " a été modifiée avec succès.");
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cett équipe n'existe pas !");
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la modification de l'équipe.");
        }
    }

    /**
     * Supprime une équipe.
     *
     * @param id L'identifiant de l'équipe à supprimer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> supprimerEquipe(Long id){
        try {
            EquipeModel equipe = equipeRepository.findById(id).orElse(null);
            for (JoueurModel joueur : equipe.getJoueurs()){
                joueur.setEquipe(null);
            }
            equipe.setNomLong("Supprimée");
            equipe.setNomCourt("Supprimée");
            equipe.setChampionnat("Pas de championnat");
            equipe.getJoueurs().clear();
            equipeRepository.save(equipe);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de la suppression de l'élément.");
        }
    }
}
package com.fan2foot.foot_api.repositorys;

import com.fan2foot.foot_api.models.JoueurModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JoueurRepository extends JpaRepository<JoueurModel,Long> {
}

package com.fan2foot.foot_api.repositorys;

import com.fan2foot.foot_api.models.RencontreModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RencontreRepository extends JpaRepository<RencontreModel,Long> {
}

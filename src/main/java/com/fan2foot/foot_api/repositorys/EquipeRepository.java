package com.fan2foot.foot_api.repositorys;

import com.fan2foot.foot_api.models.EquipeModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipeRepository extends JpaRepository<EquipeModel,Long> {
}

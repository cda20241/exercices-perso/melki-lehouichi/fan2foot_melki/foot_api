package com.fan2foot.foot_api;

import com.fan2foot.foot_api.repositorys.EquipeRepository;
import com.fan2foot.foot_api.repositorys.JoueurRepository;
import com.fan2foot.foot_api.services.ImportDonnees;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootApiApplication implements CommandLineRunner {
	private final EquipeRepository equipeRepository;
	private final JoueurRepository joueurRepository;

	public FootApiApplication(EquipeRepository equipeRepository, JoueurRepository joueurRepository) {
		this.equipeRepository = equipeRepository;
		this.joueurRepository = joueurRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		ImportDonnees importationDonneesService = new ImportDonnees(equipeRepository, joueurRepository);
		importationDonneesService.creerEquipesJoueurs();
	}

	public static void main(String[] args) {
		SpringApplication.run(FootApiApplication.class, args);
	}

}
